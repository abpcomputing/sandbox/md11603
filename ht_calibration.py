import os
import datetime
import numpy as np
B1 = True
B2 = False

if B1:
    maindir ='/nfs/cs-ccr-bqhtnfs/lhc_data/LHC.BQHT.B1/2024_03_16/'
    file_start = 'LHC.BQHT.B1_20240316'
    print("Studying B1")
if B2:
    maindir ='/nfs/cs-ccr-bqhtnfs/lhc_data/LHC.BQHT.B2/2024_03_16/'
    file_start = 'LHC.BQHT.B2_20240316'
    print("Studying B2")
file_end = '.h5'

time_string_2mm_i = "10:28:00"
time_string_2mm_f = "10:32:00"
name_2mm = "2mm"

time_string_1dot5mm_i = "10:33:00"
time_string_1dot5mm_f = "10:37:00"
name_1dot5mm = "1.5mm"

time_string_1mm_i = "10:38:00"
time_string_1mm_f = "10:43:00"
name_1mm = "1mm"

time_string_0dot5mm_i = "10:44:00"
time_string_0dot5mm_f = "10:48:00"
name_0dot5mm = "0.5mm"

time_string_0mm_i = "10:48:30"
time_string_0mm_f = "10:51:00"
name_0mm = "0mm"

time_string_minus_0dot5mm_i = "10:52:00"
time_string_minus_0dot5mm_f = "10:56:00"
name_minus_0dot5mm = "-0.5mm"

time_string_minus_1mm_i = "10:57:25"
time_string_minus_1mm_f = "11:02:00"
name_minus_1mm = "-1mm"

time_string_minus_1dot5mm_i = "11:03:00"
time_string_minus_1dot5mm_f = "11:07:00"
name_minus_1dot5mm = "-1.5mm"

time_string_minus_2mm_i = "11:08:00"
time_string_minus_2mm_f = "11:11:00"
name_minus_2mm = "-2mm"

#create an array of times_i and times_f
all_times_i = [time_string_2mm_i, time_string_1dot5mm_i, time_string_1mm_i, time_string_0dot5mm_i, time_string_0mm_i, time_string_minus_0dot5mm_i, time_string_minus_1mm_i, time_string_minus_1dot5mm_i, time_string_minus_2mm_i]
all_times_f = [time_string_2mm_f, time_string_1dot5mm_f, time_string_1mm_f, time_string_0dot5mm_f, time_string_0mm_f, time_string_minus_0dot5mm_f, time_string_minus_1mm_f, time_string_minus_1dot5mm_f, time_string_minus_2mm_f]
all_names = [name_2mm, name_1dot5mm, name_1mm, name_0dot5mm, name_0mm, name_minus_0dot5mm, name_minus_1mm, name_minus_1dot5mm, name_minus_2mm]

for ii in range(len(all_times_i)):
    all_times_i[ii] = datetime.datetime.strptime(all_times_i[ii], '%H:%M:%S')
    all_times_f[ii] = datetime.datetime.strptime(all_times_f[ii], '%H:%M:%S')

print("Starting HT Monitor analysis")
all_HT_measurements = []
for ii in range(len(all_times_i)):
    HT_measurents_i = []
    print('----------------------', all_names[ii], '----------------------')
    print('From: ', all_times_i[ii].strftime('%H:%M:%S'), ' to ', all_times_f[ii].strftime('%H:%M:%S'))
    for file in sorted(os.listdir(maindir)):
        if file.endswith(file_end):
            file_time = file.split('_')[2].split('.')[0]
            file_time = datetime.datetime.strptime(file_time, '%H%M%S')
            if file_time > all_times_i[ii] and file_time < all_times_f[ii]:
                HT_measurents_i.append(file)
                print(file)
    all_HT_measurements.append(HT_measurents_i)
