import pyjapc
import numpy as np
from cmmnbuild_dep_manager import Manager
import jpype as jp
import time

mgr = Manager()
japc = pyjapc.PyJapc('', incaAcceleratorName='LHC')
mgr.start_jpype_jvm()
japc.rbacLogin()

cern = jp.JPackage('cern')
 
print('Starting HT Monitor analysis')
if True:
	while True: 
		# japc.setParam('HX.BQHT-B1-TURN/Delay', {'delay':1})
		# japc.setParam('HX.BQHT-B2-TURN/Delay', {'delay':1})

		# japc.setParam('HX.BQHT-B1-BUNCH/Delay', {'delay':62})
		# japc.setParam('HX.BQHT-B2-BUNCH/Delay', {'delay':33})
		print('Triggering')
		japc.setParam('HX.BQHT-B1-FORCE-ARM/Trig', dict())
		japc.setParam('HX.BQHT-B2-FORCE-ARM/Trig', dict())

		print("sleep...")
		time.sleep(5)
		print("wakeup")

# japc.setParam('HX.BQHT-B1-FORCE-ARM/Trig', dict())
# japc.setParam('HX.BQHT-B2-FORCE-ARM/Trig', dict())