# MD11603

### HT Monitor calibration
On the 16 March 2024, the B1 and B2 HT Monitor calibration scans where carried out thanks to the support of G. Trad and T. Levens.

Data are in 
```
/nfs/cs-ccr-bqhtnfs/lhc_data/LHC.BQHT.B1/2024_03_16
/nfs/cs-ccr-bqhtnfs/lhc_data/LHC.BQHT.B2/2024_03_16
```
The datasets divided by bump can be retrieved via:
```
ht_calibration.py
```

The scan can be checked on the followign NXCALS snapshot:
https://timber.cern.ch/query?tab=Visualization&configuration=54422&autoLoad=true

The relevant logbook entries can be found at:
https://logbook.cern.ch/elogbook-server#/logbook?logbookId=322&dateFrom=2024-03-16T07%3A00%3A00&dateTo=2024-03-16T15%3A00%3A00

### HT Monitor automatic triggering infos
First of all install the python packages needed via:
```
install_py.sh
```

An example of the automatic HT trigger is found in:
```
ht_trig.py
```

### Useful infos
Looking at the Inspector Panel notice:

Trigger Pattern:
- Bunch Delay: this is the bunch under study;
- Repetitions: how many times the same acquisition is repeated;

Acquisition:
- Length(ms): the length of the acquisition;
- Segments: how many segments are acquired; NOTICE: it is possible to have a different number of segments and repetitions, but is a bit useless, ALWAYS set N_Repetitions = N_Segments;

Channel Gain:
These must be set to see in the HT Viewer the signal going as close as possible to the red lines limits but not too high to not distort the signal;

Trigger Arm:
- Beam Dump: if ticked automatically trigger at Beam Dump;
- HX.BQHT-CT + [] : if ticked automatically trigger at INJECTION with a delay; if left at 0 the acquistion will start 80ms (~900 turns) before the INJECTION starts.
For the trigger at Injection setup you can look at **HT_Control_INJ_Trigger.jpeg** (https://gitlab.cern.ch/abpcomputing/sandbox/md11603/-/blob/master/HT_Control_INJ_Trigger.jpeg) in this repository.

